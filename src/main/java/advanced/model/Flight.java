package advanced.model;

import lombok.Value;

/**
 * @author Andrei Petraru
 */
@Value
public class Flight {
	int number;
	String name;
}
