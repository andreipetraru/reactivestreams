package advanced.model;

import lombok.Value;

/**
 * @author Andrei Petraru
 */
@Value
public class Ticket {
	int number;
	Flight flight;
	Passenger passenger;
}
