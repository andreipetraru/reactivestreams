package advanced.model;

import lombok.Value;

/**
 * @author Andrei Petraru
 */
@Value
public class Tuple<T1, T2> {
	T1 t1;
	T2 t2;

	private Tuple(T1 t1, T2 t2) {
		this.t1 = t1;
		this.t2 = t2;
	}

	public static <T1, T2> Tuple<T1, T2> of(T1 t1, T2 t2) {
		return new Tuple<>(t1, t2);
	}
}

