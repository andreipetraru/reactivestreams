package advanced.model;

import lombok.Value;

/**
 * @author Andrei Petraru
 */

@Value
public class Passenger {
	int id;
	String name;
}
