package advanced.model;

import lombok.Builder;
import lombok.Value;

/**
 * @author Andrei Petraru
 */
@Builder
@Value
public class Email {
	String to;
	String subject;
	Ticket content;
}
