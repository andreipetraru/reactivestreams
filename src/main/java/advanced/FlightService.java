package advanced;

import advanced.model.Flight;
import advanced.model.InvalidResultException;
import advanced.model.Passenger;
import advanced.model.Ticket;
import rs.Utils;

import java.util.List;
import java.util.Objects;

public class FlightService {
	private static final List<Flight> flights = List.of(
			new Flight(1, "IAS-MIL"),
			new Flight(2, "BUC-TOR"),
			new Flight(3, "BAC-FLO"),
			new Flight(4, "SUC-CLJ"),
			new Flight(5, "BON-NYC"),
			new Flight(6, "GLA-BRA")
	);
	private static final List<Passenger> passengers = List.of(
			new Passenger(1, "Vasile"),
			new Passenger(2, "Alex"),
			new Passenger(3, "Ion"),
			new Passenger(4, "Gigi"),
			new Passenger(5, "Daniel")
	);

	private static final List<Ticket> tickets = List.of(
			new Ticket(1, flights.get(0), passengers.get(0)),
			new Ticket(2, flights.get(1), passengers.get(1)),
			new Ticket(3, flights.get(1), passengers.get(2)),
			new Ticket(4, flights.get(2), passengers.get(3)),
			new Ticket(5, flights.get(2), passengers.get(1)),
			new Ticket(6, flights.get(5), passengers.get(4))
	);

	public Flight findFlight(int number) {
		Utils.sleep(500);
		return flights.stream()
				.filter(flight -> flight.getNumber() == number)
				.findAny()
				.orElseThrow(InvalidResultException::new);
	}

	public Passenger findPassenger(int id) {
		Utils.sleep(800);
		return passengers.stream()
				.filter(p -> p.getId() == id)
				.findAny()
				.orElseThrow(InvalidResultException::new);
	}

	public Ticket findTicket(Flight flight, Passenger passenger) {
		Utils.sleep(400);
		return tickets.stream()
				.filter(ticket -> Objects.equals(ticket.getFlight(), flight))
				.filter(ticket -> Objects.equals(ticket.getPassenger(), passenger))
				.findAny()
				.orElseThrow(InvalidResultException::new);
	}

	public List<Ticket> getAll() {
		return tickets;
	}
}
