package advanced;

import reactor.core.publisher.Flux;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

public interface Twitter {
	static Flux<Status> getTweetFlux() {
		return Flux.create(subscriber -> {
			var stream = TwitterConfig.getConfiguredTwitter();
			stream.addListener(new StatusListener() {
				@Override
				public void onStatus(Status status) {
					  subscriber.next(status);
				}

				@Override
				public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

				}

				@Override
				public void onTrackLimitationNotice(int numberOfLimitedStatuses) {

				}

				@Override
				public void onScrubGeo(long userId, long upToStatusId) {

				}

				@Override
				public void onStallWarning(StallWarning warning) {

				}

				@Override
				public void onException(Exception ex) {
					subscriber.error(ex);
				}
			});
			stream.sample();
			subscriber.onCancel(stream::shutdown);
		});
	}
}

class TwitterConfig {
	static TwitterStream getConfiguredTwitter() {
		// use own auth tokens
		var cb = new ConfigurationBuilder();
		var tf = new TwitterStreamFactory(cb.build());
		return tf.getInstance();
	}
}
