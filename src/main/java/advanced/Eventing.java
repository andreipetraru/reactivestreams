package advanced;



import reactor.core.publisher.Flux;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.time.Duration;

public class Eventing {
	public static void drawFrame() {
		var jFrame = new JFrame("KeyListener");
		var lowerPanel = new JPanel();
		var jTextArea = new JTextArea(null, 6, 8);
		lowerPanel.add(jTextArea);
		jFrame.getContentPane().add(lowerPanel, "South");
		jFrame.setSize(400, 400);
		jFrame.setVisible(true);
		jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		Flux<KeyEvent> keyEvents = Flux.create(sink ->
				jTextArea.addKeyListener(new KeyListener() {
					@Override
					public void keyTyped(KeyEvent e) {
					}

					@Override
					public void keyPressed(KeyEvent e) {
						sink.next(e);
						if (e.getKeyChar() == '`') {
							sink.complete();
						}
					}

					@Override
					public void keyReleased(KeyEvent e) {
					}
				}));

		keyEvents.window(Duration.ofSeconds(1))
				.flatMap(Flux::count)
				.log()
				.subscribe();
	}

	public static void main(String[] args) {
		drawFrame();
	}
}
