package advanced.email;

import advanced.model.Email;
import advanced.model.InvalidResultException;
import advanced.model.Ticket;
import rs.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Andrei Petraru
 */
public class EmailService {
	public Email sendEmail(Ticket ticket) {
		Utils.sleep(400);
		if (Objects.equals(ticket.getNumber(), 4)) {
			throw new InvalidResultException();
		}
		return Email.builder()
				.to(ticket.getPassenger().getName())
				.subject("Your plane ticket")
				.content(ticket)
				.build();
	}

	public List<Ticket> sendEmails(List<Ticket> tickets) {
		var start = System.currentTimeMillis();
		var failures = new ArrayList<Ticket>();
		for (Ticket ticket : tickets) {
			try {
				sendEmail(ticket);
			}
			catch (InvalidResultException res) {
				failures.add(ticket);

			}
		}
		Utils.elapsedTime(start, "sequential");
		return failures;
	}
}
