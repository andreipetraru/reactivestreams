package advanced.email;

import advanced.model.Email;
import advanced.model.InvalidResultException;
import advanced.model.Ticket;
import advanced.model.Tuple;
import rs.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Andrei Petraru
 */

public class FutureEmailService {
	private final EmailService emailService;

	public FutureEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	public CompletableFuture<Email> sendEmailAsync(Ticket ticket) {
		return CompletableFuture.supplyAsync(() -> emailService.sendEmail(ticket));
	}

	public List<Ticket> sendEmails(List<Ticket> tickets) {
		var start = System.currentTimeMillis();
		var tasks = tickets.stream()
				.map(ticket -> Tuple.of(ticket, sendEmailAsync(ticket)))
				.collect(Collectors.toList());
		var results =  tasks.stream().flatMap(sendEmail()).collect(Collectors.toList());
		Utils.elapsedTime(start, "Future emails");
		return results;
	}

	public List<Ticket> sendSurprisinglySlowEmails(List<Ticket> tickets) {
		var start = System.currentTimeMillis();
		var failures = tickets.stream()
				.map(ticket -> Tuple.of(ticket, sendEmailAsync(ticket)))
				.flatMap(sendEmail())
				.collect(Collectors.toList());
		Utils.elapsedTime(start, "Slowish emails");
		return failures;
	}

	private Function<Tuple<Ticket, CompletableFuture<Email>>, Stream<? extends Ticket>> sendEmail() {
		return tuple -> {
			try {
				var future = tuple.getT2();
				future.get(500, TimeUnit.MILLISECONDS);
				return Stream.empty();
			} catch (Exception e) {
				var ticket = tuple.getT1();
				System.out.println("Could not send email " + ticket);
				return Stream.of(ticket);
			}
		};
	}
}
