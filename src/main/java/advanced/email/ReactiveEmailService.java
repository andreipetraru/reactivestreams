package advanced.email;

import advanced.model.Email;
import advanced.model.Ticket;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import rs.Utils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Andrei Petraru
 */
public class ReactiveEmailService {
	private final EmailService emailService;

	public ReactiveEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	public Mono<Email> sendEmail(Ticket ticket) {
		return Mono.fromCallable(() -> emailService.sendEmail(ticket));
	}

	// TODO: try using schedulers in both flux streams to see how it affects the result
	public List<Ticket> sendEmailsIgnoreSuccess(List<Ticket> tickets) {
		var start = System.currentTimeMillis();
		var failures = Flux.fromIterable(tickets)
				.flatMap(ticket ->
						sendEmail(ticket)
								.ignoreElement()
								.flatMap(email -> Mono.<Ticket>empty())
								.subscribeOn(Schedulers.elastic())
								.onErrorReturn(ticket)
				)
				.toStream()
				.collect(Collectors.toList());
		Utils.elapsedTime(start, "Reactive ignore success");
		return failures;
	}

	// TODO: try using schedulers in both flux streams to see how it affects the result
	public List<Ticket> sendEmails(List<Ticket> tickets) {
		var start = System.currentTimeMillis();
		var failures = Flux.fromIterable(tickets)
				.flatMap(ticket ->
						sendEmail(ticket)
								.flatMap(email -> Mono.<Ticket>empty())
								.onErrorReturn(ticket)
				)
				.toStream()
				.collect(Collectors.toList());
		Utils.elapsedTime(start, "Reactive emails");
		return failures;
	}
}
