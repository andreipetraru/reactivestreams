package rs;

import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.List;

/**
 * @author Andrei Petraru
 */

public class UserRepository {
	public List<String> findAllNames() {
		return Flux.just("ion", "gigi", "vasile")
				.delayElements(Duration.ofMillis(500))
				.collectList()
				.block();
	}

	public static Flux<List<String>> relativeNames(List<String> names) {
		return Flux.just(List.of("my cousin vinnie"), List.of("uncle bogdan"));
	}
}
