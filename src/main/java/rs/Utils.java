package rs;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Utils {
	private Utils() {
		throw new IllegalStateException();
	}

	public static void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static <T> T randomElement(List<T> list) {
		return list.get(ThreadLocalRandom.current().nextInt(list.size()));
	}

	public static void elapsedTime(long start, String name) {
		var end = System.currentTimeMillis();
		System.out.println(name + " -> " + (end - start) + "ms");
	}
}
