package rs.advanced;

import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Hooks;
import rs.UserRepository;

/**
 * @author Andrei Petraru
 */
public class Debugging {
	@Test
	public void debug() {
		Hooks.onOperatorDebug();
		var allNames = new UserRepository().findAllNames();
		var namesWithRelatives = Flux.just(allNames)
				.transform(elem -> elem.flatMap(UserRepository::relativeNames))
				.log()
				.single();

		namesWithRelatives
				.log()
				.subscribe();
	}
}
