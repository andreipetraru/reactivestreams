package rs.advanced;

import advanced.FlightService;
import advanced.email.EmailService;
import advanced.email.FutureEmailService;
import advanced.email.ReactiveEmailService;
import advanced.model.Ticket;
import org.junit.Test;

import java.util.List;

/**
 * @author Andrei Petraru
 */
public class EmailTests {
	static List<Ticket> tickets = new FlightService().getAll();
	@Test
	public void noConcurrency() {
		var service = new EmailService();
		service.sendEmails(tickets);
	}

	@Test
	public void futuresSlow() {
		var emailService = new EmailService();
		var service = new FutureEmailService(emailService);
		service.sendSurprisinglySlowEmails(tickets);
	}
	@Test
	public void futures() {
		var emailService = new EmailService();
		var service = new FutureEmailService(emailService);
		service.sendEmails(tickets);
	}

	@Test
	public void reactive() {
		var emailService = new EmailService();
		var service = new ReactiveEmailService(emailService);
		service.sendEmails(tickets);
	}

	@Test
	public void reactiveIgnoreSuccess() {
		var emailService = new EmailService();
		var service = new ReactiveEmailService(emailService);
		service.sendEmailsIgnoreSuccess(tickets);
	}
}
