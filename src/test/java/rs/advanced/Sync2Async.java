package rs.advanced;

import advanced.FlightService;
import advanced.model.Flight;
import advanced.model.Passenger;
import advanced.model.Ticket;
import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import rs.Utils;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;

/**
 * @author Andrei Petraru
 * <p>
 * 1 - find ticket for flight and passenger
 * 2 - send email to all passengers, return all email failures
 */
public class Sync2Async {
	@Test
	public void sequential() {
		var start = System.currentTimeMillis();
		var flightService = new FlightService();
		var passenger = flightService.findPassenger(1);
		var flight = flightService.findFlight(1);
		var ticket = flightService.findTicket(flight, passenger);
		Utils.elapsedTime(start, "sequential");
	}

	@Test
	public void futures() {
		var start = System.currentTimeMillis();
		var flightService = new FlightService();
		var passenger = CompletableFuture.supplyAsync(() -> flightService.findPassenger(1));
		var flight = CompletableFuture.supplyAsync(() -> flightService.findFlight(1));
		var ticket = passenger.thenCombine(flight, (f, p) -> flightService.findTicket(p, f));
		ticket.join();
		Utils.elapsedTime(start, "futures");
	}

	@Test
	public void reactive() {
		var start = System.currentTimeMillis();
		var flightService = new FlightService();
		var reactiveService = new Reactive(flightService);
		var pass = reactiveService.getPassenger(1).subscribeOn(Schedulers.elastic());
		var flight = reactiveService.getFlight(1).subscribeOn(Schedulers.elastic());
		var ticket = Flux.zip(pass, flight, (p, f) -> reactiveService.getTicket(f, p));
		Ticket ticketFlux = ticket.flatMap(x -> x).blockFirst();
		Utils.elapsedTime(start, "reactive");
	}

	class Reactive {
		private final FlightService flightService;

		Reactive(FlightService flightService) {
			this.flightService = flightService;
		}

		Flux<Passenger> getPassenger(int id) {
			return Flux.defer(() -> Flux.just(flightService.findPassenger(id)));
		}

		Flux<Flight> getFlight(int id) {
			return Flux.defer(() -> Flux.just(flightService.findFlight(id)));
		}

		Flux<Ticket> getTicket(Flight flight, Passenger passenger) {
			return Flux.defer(() -> Flux.just(flightService.findTicket(flight, passenger)));
		}
	}

	@Test
	public void async() {
		Flux.interval(Duration.ofMillis(50))
				.take(5)
				.log()
				.blockLast();
	}

	@Test
	public void sync() {
		Flux.just(1, 2 ,3)
				.log()
				.subscribeOn(Schedulers.parallel())
				.subscribe();



		Utils.sleep(100);
	}
}
