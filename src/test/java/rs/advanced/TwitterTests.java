package rs.advanced;

import advanced.Twitter;
import org.junit.Test;
import rs.Utils;
import twitter4j.Status;

public class TwitterTests {
	public static final String TRUMP = "trump".toUpperCase();
	@Test
	public void tweets() {
		var twitter = Twitter.getTweetFlux();
		var sub1 = twitter
				.filter(t -> t.getText().toUpperCase().contains(TRUMP))
				.subscribe(s -> print(s, "SUB1"));
		Utils.sleep(2000);
		var sub2 = twitter
				.filter(t -> t.getText().toUpperCase().contains(TRUMP))
				.subscribe(s -> print(s, "SUB2"));
		Utils.sleep(10000);
		sub1.dispose();
		Utils.sleep(5000);
		sub2.dispose();
	}

	void print(Status status, String name) {
		System.out.println(name + " " + status.getText());
	}
}
