package rs.backpressure;

import org.junit.Test;
import reactor.core.publisher.Flux;
import rs.Utils;

import java.time.Duration;

/**
 * @author Andrei Petraru
 */
public class Sampling {
	@Test
	public void sample() {
		Flux.interval(Duration.ofMillis(5))
				.sample(Duration.ofMillis(20))
				.subscribe(System.out::println);

		Utils.sleep(500);
	}

	@Test
	public void buffer() {
		Flux.interval(Duration.ofMillis(5))
				.buffer(10)
				.log()
				.subscribe();
		Utils.sleep(1000);
	}

	@Test
	public void window() {
		Flux.interval(Duration.ofMillis(5))
				.window(20)
				.flatMap(Flux::count)
				.subscribe(System.out::println);
		Utils.sleep(500);
	}
}
