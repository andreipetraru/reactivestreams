package rs.backpressure;

import lombok.Value;
import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import rs.Utils;

import java.time.Duration;

/**
 * @author Andrei Petraru
 */
public class BackPressure {
	@Test
	public void backpressure() {
		Flux.interval(Duration.ofMillis(10))
				.map(x -> new User(x, "gigi", "898989"))
				.log()
				.limitRate(20)
				.subscribeOn(Schedulers.parallel())
				.subscribe();

		Utils.sleep(1000);
	}
}

@Value
class User {
	long id;
	String name;
	String phoneNo;
}
