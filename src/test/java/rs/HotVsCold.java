package rs;

import org.junit.Test;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class HotVsCold {
	@Test
	public void cold() {
		var flux = Flux.range(1, 5);
		flux.log().subscribe();
		flux.log().subscribe();
	}

	@Test
	public void colder() {
		var flux = Flux.range(1, 50)
				.delayElements(Duration.ofMillis(50)).share();
		var f1 = flux.log().subscribe();

		Utils.sleep(1000);
		var f2 = flux.log().subscribe();


		f1.dispose();
		Utils.sleep(2000);
		f2.dispose();
	}
}
