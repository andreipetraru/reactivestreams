package rs.basic;

import org.junit.Test;
import reactor.core.publisher.Flux;

public class Transform {
	Flux<Integer> range = Flux.range(1, 10);

	@Test
	public void skip() {
	}

	@Test
	public void take() {
	}

	@Test
	public void map() {
	}

	@Test
	public void flatMap() {
		range.flatMap(x -> Flux.just(x));
	}

	@Test
	public void buffer() {
	}

	@Test
	public void collect() {
	}
}
