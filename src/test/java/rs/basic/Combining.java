package rs.basic;

import org.junit.Test;
import reactor.core.publisher.Flux;

import java.time.Duration;

import static rs.Utils.sleep;

public class Combining {
	Flux<String> fast = Flux.interval(Duration.ofMillis(10))
			.map(num -> "Fast - " + num);
	Flux<String> slow = Flux.interval(Duration.ofMillis(50))
			.map(num -> "Slow - " + num);

	@Test
	public void simple() {
		Flux.just(1, 2, 3)
				.map(x -> {
					if (x == 2) {
						throw new IllegalStateException();
					}
					return x;
				})
				.subscribe();
	}

	@Test
	public void merge() {
		var first = Flux.just("1", "2", "3");
		var second = Flux.just("4", "5");
		Flux.merge(first, second)
				.log();
//				.subscribe();
	}

	@Test
	public void zip() {
		var first = Flux.just("1", "2", "3");
		var second = Flux.just("4", "5");
		var third = Flux.just("6", "7", "8");
		Flux.zip(first, second, third)
				.log()
				.subscribe();
	}

	@Test
	public void mergeTemporal() {
		Flux.merge(slow, fast)
				.log()
				.subscribe();

		sleep(200);
	}

	@Test
	public void zipTemporal() {
		// interval doesn't support small downstream requests that replenish slower than the ticks)
		Flux.zip(slow, fast)
				.log()
				.subscribe();

		sleep(2000);
	}

	@Test
	public void zipTemporalSlowVsFastStream() {
		var fastFlux = Flux.range(1, 1_000_000)
				.delayElements(Duration.ofMillis(5));
		var slowFlux = Flux.range(1, 1_000_000)
				.delayElements(Duration.ofMillis(500));
		Flux.zip(fastFlux, slowFlux)
				.log()
				.subscribe();

		sleep(20000);
	}

	@Test
	public void combineLatest() {
		Flux<String> fast1 = Flux.interval(Duration.ofMillis(10))
				.map(num -> "Fast - " + num);
		Flux<String> slow1 = Flux.interval(Duration.ofMillis(50))
				.map(num -> "Slow - " + num);

		Flux.combineLatest(slow1, fast1, (s, f) -> s + f)
				.log()
				.subscribe();


		sleep(2000);
	}

	@Test
	public void withLatestFrom() {

		sleep(2000);
	}
}
