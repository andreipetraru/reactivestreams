package rs.basic;

import org.junit.Test;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.List;
import java.util.stream.Stream;

public class Creation {
	@Test
	public void creation() {
		Flux.empty();

		Flux.just("1", "1", "2");

		Flux.fromArray(new String[]{"Apple", "Orange", "Grape"});

		Flux.fromStream(Stream.of(1, 2, 3));

		Flux.fromIterable(List.of(3, 2, 1));

		Flux.range(1, 100);

		Flux.interval(Duration.ofMillis(5));

		Flux.create(sink -> {
			List<Integer> integers = List.of(1, 2, 3);
			try {
				integers.forEach(sink::next);
			}
			catch (Exception e) {
				sink.error(e);
			}
			sink.complete();
		});
	}
}
