package rs;

import org.junit.Test;

import java.util.stream.IntStream;

public class StreamLimitations {
	@Test
	public void notRepeatable() {

	}

	@Test
	public void notForkable() {

	}

	@Test
	public void goodLuckExceptions() {
		var stream = getStream();
			stream.map(x -> 10 / x).sum();
	}

	IntStream getStream() {
		return IntStream.range(0, 100);
	}
}
